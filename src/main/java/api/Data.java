package api;

import java.io.IOException;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

public class Data implements DataSerializable {
	
	private int activityCondition;
	private int homeId;
	private String department;
	private String province;
	
//	@Override
	public void readData(ObjectDataInput arg0) throws IOException {
		// TODO Auto-generated method stub
		activityCondition=arg0.readInt();
		homeId=arg0.readInt();
		department=arg0.readUTF();
		province=arg0.readUTF();
		
	}

//	@Override
	public void writeData(ObjectDataOutput arg0) throws IOException {
		arg0.writeInt(activityCondition);
		arg0.writeInt(homeId);
		arg0.writeUTF(department);
		arg0.writeUTF(province);
		
	}

	public int getActivityCondition() {
		return activityCondition;
	}

	public void setActivityCondition(int activityCondition) {
		this.activityCondition = activityCondition;
	}

	public int getHomeId() {
		return homeId;
	}

	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "Data [activityCondition=" + activityCondition + ", homeId=" + homeId + ", department=" + department
				+ ", province=" + province + "]";
	}

	public String getRegion() {
		String region;
		String RegionBuenosAires = "Buenos Aires|Ciudad Autónoma de Buenos Aires";
		String RegionCentro = "Córdoba|Santa Fe|Entre Ríos";
		String RegionDelNuevoCuyo = "Mendoza|San Juan|San Luis|La Rioja";
		String RegionPatagonica = "La Pampa|Río negro|Chubut|Neuquén|Santa Cruz|Tierra del Fuego";

		if (RegionBuenosAires.contains(province)) {
			region = "Región Buenos Aires";
		} else if (RegionCentro.contains(province)) {
			region = "Región Centro";
		} else if (RegionDelNuevoCuyo.contains(province)) {
			region = "Región del Nuevo Cuyo";
		} else if (RegionPatagonica.contains(province)) {
			region = "Región Patagónica";
		} else {
			region = "Región del Norte Grande Argentino";
		}
		return region;
	}
}
