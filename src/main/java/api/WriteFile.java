package api;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class WriteFile {
	private String filename;
	
	public WriteFile(String filename) {
		this.filename = filename;
	}
	
	public void doWrite(List<String> list) {
		Path file = Paths.get(this.filename);
		try {
			Files.write(file, list, Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void doWrite(Map<String, Integer> map) {
		Path file = Paths.get(this.filename);
		List<String> list = new ArrayList<String>();
		
		map.forEach( (k, v) -> list.add(k + ", " + v));
		
		try {
			Files.write(file, list, Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void write(Map<String, Float> map) {
		Path file = Paths.get(this.filename);
		List<String> list = new ArrayList<String>();
		
		map.forEach( (k, v) -> list.add(k + ", " + v));
		
		try {
			Files.write(file, list, Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
