package ar.edu.itba.pod.hazelClient;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class PopulationReducer implements ReducerFactory<String,Integer,Integer> {



//	@Override
	public Reducer<Integer, Integer> newReducer(String arg0) {
		// TODO Auto-generated method stub
		return new Reducer<Integer,Integer>(){
			private int count=0;

			@Override
			public Integer finalizeReduce() {
				// TODO Auto-generated method stub
				return count;
			}

			@Override
			public void reduce(Integer arg0) {
				count+=arg0;
				
			}
			
		};
	}

}
