package ar.edu.itba.pod.hazelClient;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import api.Data;
import api.WriteFile;
import ar.edu.itba.pod.query02.ProvinceCollector;
import ar.edu.itba.pod.query02.Query02Mapper;
import ar.edu.itba.pod.query02.Query02Reduce;
import ar.edu.itba.pod.query03.Query03Mapper;
import ar.edu.itba.pod.query03.Query03Reducer;
import ar.edu.itba.pod.query04.HomeRegion;
import ar.edu.itba.pod.query04.Query04Mapper;
import ar.edu.itba.pod.query04.Query04Reduce;
import ar.edu.itba.pod.query05.Query05Mapper;
import ar.edu.itba.pod.query05.Query05Reducer;
import ar.edu.itba.pod.query06.Query06Collator;
import ar.edu.itba.pod.query06.Query06Mapper;
import ar.edu.itba.pod.query06.Query06Reduce;
import ar.edu.itba.pod.query07.Query07Mapper;
import ar.edu.itba.pod.query07.Query07Reducer;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;

public class Client {
	private static HazelcastInstance client;
	private static final String MAP = "54623-51158-51274-50386";
	private static ArgumentParser params;
	final static Logger logger = Logger.getLogger(Client.class);
	private static WriteFile writeFile;
	
    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
//    	ClientConfig cfg = new ClientConfig();
//    	ClientNetworkConfig ncfg = new ClientNetworkConfig();
//    	ncfg.addAddress("127.0.0.1:5701","127.0.0.1:5702");
//    	cfg.setNetworkConfig(ncfg);
//    	client = HazelcastClient.newHazelcastClient(cfg);

		// ARGUMENT PARSER
		// TODO Revisar las llamadas Client para que el parseo no falle
		params = new ArgumentParser();

		try
		{
			params.parseArguments();
		}
		catch (IllegalArgumentException e)
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}

		writeFile = new WriteFile(params.getOutPath());
//		System.setProperty("log4jfilename", params.getTimeOutPath());
		//TODO Ejemplo de uso
		//Java -Daddresses=xx.xx.xx.xx;yy.yy.yy.yy	-Dquery=1 -DinPath=censo.csv -DoutPath=output.txt
		// 											-DtimeOutPath=time.txt -Dprov=Cordoba -Dn=10 client.myClient
		/*
		System.out.println("Address 1: " + params.getAddresses()[0]);
        System.out.println("Address 2: " + params.getAddresses()[1]);
        System.out.println("Query: " + params.getQuery());
        System.out.println("In Path: " + params.getInPath());
        System.out.println("Out Path: " + params.getOutPath());
        System.out.println("Time Out Path: " + params.getTimeOutPath());
        System.out.println("Prov: " + params.getProv());
        System.out.println("N: " + params.getN());
		*/

    	client= buildCluster(1);

		logger.info("Inicio de la lectura del archivo " + params.getInPath());

    	IMap<Integer,Data> dataMap = client.getMap(MAP);
    	Reader in = new FileReader(params.getInPath());
//    	Reader in = new FileReader("census100.csv");
    	Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
    	int i=0;
    	
    	for (CSVRecord record : records) {
    	    String activityCondition = record.get(0);
    	    String homeId = record.get(1);
    	    String department = record.get(2);
    	    String province = record.get(3);
    	    Data d = new Data();
    	    d.setActivityCondition(Integer.parseInt(activityCondition));
    	    d.setHomeId(Integer.parseInt(homeId));
    	    d.setDepartment(department);
    	    d.setProvince(province);
    	    dataMap.set(i,d);
    	    i++;
    	}
    	in.close();
		logger.info("Fin de lectura del archivo " + params.getInPath());

		logger.info("Inicio del trabajo map/reduce de la QUERY-" + params.getQuery());
    	switch(params.getQuery())
		{
			case 1:
				firstQuery(dataMap);
				break;
			case 2:
				secondQuery(dataMap, params.getN(), params.getProv());
				break;
			case 3:
				thirdQuery(dataMap);
				break;
			case 4:
				fourthQuery(dataMap);
				break;
			case 5:
				fifthQuery(dataMap);
				break;
			case 6:
				sixthQuery(dataMap, params.getN());
				break;
			case 7:
				seventhQuery(dataMap, params.getN());
				break;
			default:
				break;
		}
		logger.info("Fin del trabajo map/reduce de la QUERY-" + params.getQuery());

    }
    
	private static void firstQuery(IMap<Integer, Data> dataMap) throws InterruptedException, ExecutionException {
		JobTracker tracker = client.getJobTracker("default");
		KeyValueSource<Integer,Data> source = KeyValueSource.fromMap(dataMap);
		Job<Integer,Data> job = tracker.newJob(source);
		ICompletableFuture<Map<String,Integer>> futureQuery = job.mapper(new RegionMapper()).reducer(new PopulationReducer()).submit();
		Map<String,Integer> result = futureQuery.get();

		Map<String, Integer> ordered = new HashMap<>(result);
		ordered = ordered.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		ordered.forEach( (k, v) -> System.out.println(k + ", " + v));
		writeFile.doWrite(ordered);
	}

	private static void secondQuery(IMap<Integer, Data> dataMap, int n, String prov) throws InterruptedException, ExecutionException {
		JobTracker tracker = client.getJobTracker("default");
		KeyValueSource<Integer,Data> source = KeyValueSource.fromMap(dataMap);
		Job<Integer,Data> job = tracker.newJob(source);
        ICompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query02Mapper())
                .reducer(new Query02Reduce())
                .submit(new ProvinceCollector(n, prov));
		Map<String,Integer> result = future.get();

		Map<String, Integer> ordered = new HashMap<>(result);
		ordered = ordered.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		ordered.forEach( (k, v) -> System.out.println(k + ", " + v));
		writeFile.doWrite(ordered);
    }

	private static void thirdQuery(IMap<Integer, Data> dataMap) throws InterruptedException, ExecutionException {
		JobTracker tracker = client.getJobTracker("default");
		KeyValueSource<Integer,Data> source = KeyValueSource.fromMap(dataMap);
		Job<Integer,Data> job = tracker.newJob(source);
		ICompletableFuture<Map<String,Float>> futureQuery = job.mapper(new Query03Mapper()).reducer(new Query03Reducer()).submit();
		Map<String,Float> result = futureQuery.get();

		System.out.println("Indices de desempleo:");
		Map<String, Float> ordered = new HashMap<>(result);
		ordered = ordered.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		ordered.forEach( (k, v) -> System.out.printf(k + ", %.2f \n",  v));
		writeFile.write(ordered);
	}
	
	private static void fourthQuery(IMap<Integer, Data> dataMap) throws InterruptedException, ExecutionException {
		JobTracker tracker = client.getJobTracker("default");
		KeyValueSource<Integer,Data> source = KeyValueSource.fromMap(dataMap);
		Job<Integer,Data> job = tracker.newJob(source);
        ICompletableFuture<Map<HomeRegion, Integer>> future = job
                .mapper(new Query04Mapper())
                .reducer(new Query04Reduce())
                .submit();
		Map<HomeRegion,Integer> result = future.get();

		List<Entry<HomeRegion,Integer>> ret = new ArrayList<>();
		ret.addAll(result.entrySet());

        Collections.sort(ret, new Comparator<Map.Entry<HomeRegion,Integer>>() {
            public int compare(Map.Entry<HomeRegion,Integer> o1, Map.Entry<HomeRegion,Integer> o2) {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        });
		
        List<String> res = new ArrayList<String>();
		for (Entry<HomeRegion,Integer> e: ret){
			System.out.println(e.getKey().getRegion()+"|"+e.getValue());
			res.add(e.getKey().getRegion()+", "+e.getValue());
		}
		
		writeFile.doWrite(res);
	}

	private static void fifthQuery(IMap<Integer, Data> dataMap) throws InterruptedException, ExecutionException {
		JobTracker tracker = client.getJobTracker("default");
		KeyValueSource<Integer,Data> source = KeyValueSource.fromMap(dataMap);
		Job<Integer,Data> job = tracker.newJob(source);
		ICompletableFuture<Map<String,Float>> futureQuery = job
				.mapper(new Query05Mapper())
				.reducer(new Query05Reducer())
				.submit();
		Map<String,Float> result = futureQuery.get();

		System.out.println("Habitantes por region:");

		Map<String, Float> ordered = new HashMap<>(result);
		ordered = ordered.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		ordered.forEach( (k, v) -> System.out.printf(k + ", %f\n",  v));
		writeFile.write(ordered);
	}

	private static void sixthQuery(IMap<Integer, Data> dataMap, int diffProvinces) throws InterruptedException, ExecutionException {
		JobTracker tracker = client.getJobTracker("default");
		KeyValueSource<Integer,Data> source = KeyValueSource.fromMap(dataMap);
		Job<Integer,Data> job = tracker.newJob(source);
        ICompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query06Mapper())
                .reducer(new Query06Reduce())
                .submit(new Query06Collator(diffProvinces));
		
        Map<String, Integer> result = future.get();

		List<Entry<String, Integer>> ret = new ArrayList<>();
		ret.addAll(result.entrySet());

       Collections.sort(ret, new Comparator<Map.Entry<String,Integer>>() {
            public int compare(Map.Entry<String,Integer> o1, Map.Entry<String,Integer> o2) {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        });

       List<String> res = new ArrayList<String>();
		for (Entry<String,Integer> e: ret){
			System.out.println(e.getKey()+"|"+e.getValue());
			res.add(e.getKey()+", "+e.getValue());
		}
		writeFile.doWrite(res);
	}

	private static void seventhQuery(IMap<Integer, Data> dataMap, int n) throws InterruptedException, ExecutionException {
		JobTracker tracker = client.getJobTracker("default");
		KeyValueSource<Integer,Data> source = KeyValueSource.fromMap(dataMap);
		Job<Integer,Data> job = tracker.newJob(source);
		ICompletableFuture<Map<String,Set<String>>> futureQuery = job
				.mapper(new Query07Mapper())
				.reducer(new Query07Reducer())
				.submit();
		Map<String,Set<String>> result = futureQuery.get();

		Iterator<String> iter = result.keySet().iterator();
		Map<String, Integer> ordered = new HashMap<>();

		while(iter.hasNext()){
			String current = iter.next();
			Set<String> departments = result.get(current);
			iter.remove();
			for(String province: result.keySet()){
				Set<String> intersection = new HashSet<>(departments);
				intersection.retainAll(result.get(province));
				if(intersection.size() >= n){
					ordered.put(current + " + " +province + ", ", intersection.size());
				}
			}
		}
		ordered = ordered.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
				(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		ordered.forEach( (k, v) -> System.out.println(k+v));
		writeFile.doWrite(ordered);
	}

	//no es necesario para mas adelante
    private static HazelcastInstance buildCluster(int memberCount) {
        ClientConfig config = new ClientConfig();
        ClientNetworkConfig networkConfig = config.getNetworkConfig();
        networkConfig.setAddresses(Arrays.asList(params.getAddreses()));
        config.getGroupConfig().setName(MAP);
        return HazelcastClient.newHazelcastClient(config);
       /* networkConfig.getJoin().getMulticastConfig().setEnabled(false);
        networkConfig.getJoin().getTcpIpConfig().setEnabled(true);
        networkConfig.getJoin().getTcpIpConfig().setMembers(Arrays.asList(new String[]{"127.0.0.1"}));

        HazelcastInstance[] hazelcastInstances = new HazelcastInstance[memberCount];
        for (int i = 0; i < memberCount; i++) {
            hazelcastInstances[i] = Hazelcast.newHazelcastInstance(config);
        }
        return hazelcastInstances[0];*/
    }
	
}
