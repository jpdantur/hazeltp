package ar.edu.itba.pod.hazelClient;

public class ArgumentParser {

    private static final String addresesDefault = "127.0.0.1";
    private static final String inPathDefault = "census100.csv";
    private static final String outPathDefault = "output.txt";
    private static final String timePathDefault = "time.txt";

    private String[] addresses;
    private int query;
    private String inPath;
    private String outPath;
    private String timeOutPath;
    private int n;
    private String prov;


    public ArgumentParser() {
        super();
    }

    public void parseArguments() throws IllegalArgumentException
    {
        // Argument - Addresses
        try
        {
            String strAddresses = System.getProperty("addresses", addresesDefault);
            this.addresses = strAddresses.split("[,;]");
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Ilegal Argument: \'addresses\'.");
        }

        // Argument - Query
        try
        {
            String strQuery = System.getProperty("query");

            if (strQuery == null)
            {
                throw new IllegalArgumentException();
            }

            int queryNumber = Integer.valueOf(strQuery);

            // Check that query is between accepted range
            if (queryNumber < 1 || queryNumber > 7)
            {
                throw new IllegalArgumentException();
            }

            this.query = queryNumber;
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Ilegal Argument: \'query\'. Argument is required and only numeric values between 1 and 7 are accepted.");
        }

        // Argument - In Path
        try
        {
            this.inPath = System.getProperty("inPath", inPathDefault);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Ilegal Argument: \'inPath\'.");
        }

        // Argument - Out Path
        try
        {
            this.outPath = System.getProperty("outPath", outPathDefault);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Ilegal Argument: \'outPath\'.");
        }

        // Argument - Time Out Path
        try
        {
            this.timeOutPath = System.getProperty("timePath", timePathDefault);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Ilegal Argument: \'timePath\'.");
        }

        switch (getQuery())
        {
            case 2:
                // Argument - Prov
                try
                {
                    this.prov = System.getProperty("prov");
                    if (getProv() == null)
                    {
                        throw new IllegalArgumentException();
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException("Ilegal Argument: \'prov\'. Valid argument is required.");
                }
            case 6:
            case 7:
                // Argument - N
                try
                {
                    String strN = System.getProperty("n");

                    if (strN == null)
                    {
                        throw new IllegalArgumentException();
                    }

                    this.n = Integer.valueOf(strN);
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException("Ilegal Argument: \'n\'. Argument is required and only numeric values are accepted.");
                }
                break;
            default:
                break;
        }
    }

    public String getInPath() {
        return inPath;
    }

    public String getOutPath() {
        return outPath;
    }

    public String getTimeOutPath() {
        return timeOutPath;
    }

    public int getN() {
        return n;
    }

    public String getProv() {
        return prov;
    }

    public String[] getAddreses() {
        return addresses;
    }

    public int getQuery() {
        return query;
    }
}