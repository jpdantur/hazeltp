package ar.edu.itba.pod.query03;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query03Reducer implements ReducerFactory<String,Integer,Float> {

    public Reducer<Integer, Float> newReducer(String keyIn) {

        return new Reducer<Integer,Float>(){
            private int unemployed=0;
            private int employed=0;

            @Override
            public Float finalizeReduce() {
                return unemployed / ((float)employed + unemployed);
            }

            @Override
            public void reduce(Integer employmentStatus) {
                if(employmentStatus == 1) employed++;
                if(employmentStatus == 2) unemployed++;
            }
        };
    }
}
