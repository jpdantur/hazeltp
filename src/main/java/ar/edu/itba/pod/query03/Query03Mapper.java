package ar.edu.itba.pod.query03;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

import api.Data;


public class Query03Mapper implements Mapper<Integer, Data, String, Integer> {

//	@Override
        public void map(Integer keyIn, Data data, Context<String, Integer> context) {
            context.emit(data.getRegion(), data.getActivityCondition());
        }
}
