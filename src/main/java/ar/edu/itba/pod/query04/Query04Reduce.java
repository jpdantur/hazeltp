package ar.edu.itba.pod.query04;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query04Reduce implements ReducerFactory<HomeRegion,Integer,Integer> {

	@Override
	public Reducer<Integer, Integer> newReducer(HomeRegion key) {
		return new Query04Reducer();
	}
	
	private static class Query04Reducer extends Reducer<Integer, Integer> {

		private volatile int count;

		@Override
		public void reduce(Integer value) {
			count += value;
		}

		@Override
		public Integer finalizeReduce() {
			return count == 0 ? null : count;
		}
	}

}
