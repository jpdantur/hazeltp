package ar.edu.itba.pod.query04;

import api.Data;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query04Mapper implements Mapper<Integer, Data, HomeRegion, Integer> {

	@Override
	public void map(Integer key, Data value, Context<HomeRegion, Integer> context) {
		context.emit(new HomeRegion(value.getHomeId(), value.getRegion()), 1);
	}

}
