package ar.edu.itba.pod.query06;

import api.Data;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query06Mapper implements Mapper<Integer, Data, String, String> {

	@Override
	public void map(Integer key, Data value, Context<String, String> context) {
		context.emit(value.getDepartment(), value.getProvince());
	}
}
