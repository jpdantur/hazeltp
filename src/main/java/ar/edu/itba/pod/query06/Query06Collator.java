package ar.edu.itba.pod.query06;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.hazelcast.mapreduce.Collator;

public class Query06Collator implements Collator<Entry<String, List<String>>, Map<String, Integer>> {
	private int diffProvinces;
	
	public Query06Collator(int provinces) {
		this.diffProvinces = provinces;
	}
	
	@Override
	public Map<String, Integer> collate(Iterable<Entry<String, List<String>>> values) {
		Map<String, Integer> ret = new HashMap<>();
		
		for(Entry<String, List<String>> entry: values) {
			if(entry.getValue().stream().distinct().count() >= this.diffProvinces) {
				System.out.println(new ArrayList<>(new HashSet<>(entry.getValue())));
				ret.put(entry.getKey(), entry.getValue().size());				
			}
		}
		
//		list = list.subList(0, n);
//		
//		Map<String, Integer> ret = new HashMap<>();
//		for (int i = 0; i < list.size() && i < this.n; i++) {
//			Entry<Province, Integer> en = list.get(i);
//			ret.put(en.getKey().getDepartment(), en.getValue());
//		}
		return ret;
	}
}
	
