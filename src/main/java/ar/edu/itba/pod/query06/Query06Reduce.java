package ar.edu.itba.pod.query06;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query06Reduce implements ReducerFactory<String, String, List<String>>{


	@Override
	public Reducer<String, List<String>> newReducer(String key) {
		return new Query06Reducer();
	}	

	private static class Query06Reducer extends Reducer<String, List<String>> {

		private List<String> provinces;
		@Override
		public void beginReduce() {
			provinces = new ArrayList<String>();
		};
		
		@Override
		public void reduce(String value) {
			provinces.add(value);
		}

		@Override
		public List<String> finalizeReduce() {
			return provinces;
		}
	}

}
