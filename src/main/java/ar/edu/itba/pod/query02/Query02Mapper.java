package ar.edu.itba.pod.query02;

import api.Data;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query02Mapper implements Mapper<Integer, Data, Province, Integer> {

	@Override
	public void map(Integer key, Data value, Context<Province, Integer> context) {
		context.emit(new Province(value.getProvince(), value.getDepartment(), value.getHomeId()), 1);		
	}

}
