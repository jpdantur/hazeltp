package ar.edu.itba.pod.query02;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.hazelcast.mapreduce.Collator;

public class ProvinceCollector implements Collator<Entry<Province, Integer>, Map<String, Integer>> {

	private int n;
	private String prov;
	
	
	public ProvinceCollector(int n, String prov) {
		this.n = n;
		this.prov = prov;
	}
	
	@Override
	public Map<String, Integer> collate(Iterable<Entry<Province, Integer>> values) {
		List<Entry<Province, Integer>> list = new ArrayList<>();
		
		for(Entry<Province, Integer> entry: values) {
			if(entry.getKey().getProvince().equals(this.prov)) {
				list.add(entry);				
			}
		}
		
		list = list.subList(0, n);
		
		Map<String, Integer> ret = new HashMap<>();
		for (int i = 0; i < list.size() && i < this.n; i++) {
			Entry<Province, Integer> en = list.get(i);
			ret.put(en.getKey().getDepartment(), en.getValue());
		}
		return ret;
	}
	

	
}