package ar.edu.itba.pod.query02;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query02Reduce implements ReducerFactory<Province,Integer,Integer> {

	@Override
	public Reducer<Integer, Integer> newReducer(Province key) {
		return new Query02Reducer();
	}

	private static class Query02Reducer extends Reducer<Integer, Integer> {

		private volatile int count;

		@Override
		public void reduce(Integer value) {
			count += value;
		}

		@Override
		public Integer finalizeReduce() {
			return count == 0 ? null : count;
		}
	}


}
