package ar.edu.itba.pod.query02;

import java.io.Serializable;

public class Province implements Serializable {

	String department;
	String province;
	int homeId;
	
	public Province(String province, String department, int homeId) {
		this.province = province;
		this.department = department;
		this.homeId = homeId;
	}

	public int getHomeId() {
		return homeId;
	}
	
	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "Provincia [department=" + department + ", province=" + province
				+ ", homeId=" + homeId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((department == null) ? 0 : department.hashCode());
		result = prime * result
				+ ((province == null) ? 0 : province.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Province other = (Province) obj;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (province == null) {
			if (other.province != null)
				return false;
		} else if (!province.equals(other.province))
			return false;
		return true;
	}



}
