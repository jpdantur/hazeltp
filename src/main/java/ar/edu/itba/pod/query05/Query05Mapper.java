package ar.edu.itba.pod.query05;

import api.Data;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query05Mapper implements Mapper<Integer, Data, String, Integer> {

    @Override
    public void map(Integer keyIn, Data data, Context<String, Integer> context) {
        context.emit(data.getRegion(), data.getHomeId());
    }
}
