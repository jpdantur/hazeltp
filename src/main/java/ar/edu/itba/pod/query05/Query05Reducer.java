package ar.edu.itba.pod.query05;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

import java.util.HashSet;
import java.util.Set;

public class Query05Reducer implements ReducerFactory<String, Integer, Float> {

    @Override
    public Reducer<Integer, Float> newReducer(String region) {
        return new Reducer<Integer, Float>() {

            private Set<Integer> prom;
            private long count = 0;

            @Override
            public void beginReduce() {
                prom = new HashSet<>();
            }

            @Override
            public void reduce(Integer hogarId) {
                prom.add(hogarId);
                count++;
            }

            @Override
            public Float finalizeReduce() {
                return (float) count / prom.size();
            }
        };
    }
}
