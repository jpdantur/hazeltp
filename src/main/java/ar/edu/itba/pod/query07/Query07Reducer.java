package ar.edu.itba.pod.query07;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

import java.util.HashSet;
import java.util.Set;

public class Query07Reducer implements ReducerFactory<String, String, Set<String>> {

    @Override
    public Reducer<String, Set<String>> newReducer(String region) {
        return new Reducer<String, Set<String>>() {

            private Set<String> departments = new HashSet<>();

            @Override
            public void reduce(String department) {
                departments.add(department);
            }

            @Override
            public Set<String> finalizeReduce() {
                return departments;
            }
        };
    }
}
