package ar.edu.itba.pod.query07;

import api.Data;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query07Mapper implements Mapper<Integer, Data, String, String> {

    @Override
    public void map(Integer keyIn, Data data, Context<String, String> context) {
        context.emit(data.getProvince(), data.getDepartment());
    }
}
