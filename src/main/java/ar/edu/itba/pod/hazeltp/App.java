package ar.edu.itba.pod.hazeltp;

import java.util.Arrays;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentMap;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MemberAddressProviderConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		Config cfg = new Config();
		cfg.getGroupConfig().setName("54623-51158-51274-50386");
		HazelcastInstance instance1 = Hazelcast.newHazelcastInstance(cfg);
		HazelcastInstance instance2 = Hazelcast.newHazelcastInstance(cfg);
		HazelcastInstance instance3 = Hazelcast.newHazelcastInstance(cfg);
	}
}
